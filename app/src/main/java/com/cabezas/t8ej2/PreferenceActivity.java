package com.cabezas.t8ej2;

import android.app.Activity;
import android.os.Bundle;

public class PreferenceActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preference);
        getFragmentManager().beginTransaction().replace(android.R.id.content, new
                OptionsFragment()).commit();
    }


}
