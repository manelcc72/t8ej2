package com.cabezas.t8ej2;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends Activity implements SharedPreferences.OnSharedPreferenceChangeListener {

    private TextView mTvNombre;
    private ImageView mIvImagen;
    private SharedPreferences sp;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        mTvNombre = (TextView) findViewById(R.id.txt);
        mIvImagen = (ImageView) findViewById(R.id.img);

        // Obtenemos las preferencias almacenadas existentes
        sp = PreferenceManager.getDefaultSharedPreferences(this);
        sp.registerOnSharedPreferenceChangeListener(this);

        mTvNombre.setText("Bienvenido " + sp.getString("PREF_NAME", "Imagina Group"));
        if (sp.getBoolean("PREF_SHOW_IMAGE", true)) {
            mIvImagen.setVisibility(ImageView.VISIBLE);
        } else {
            mIvImagen.setVisibility(ImageView.INVISIBLE);
        }


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

            startActivity(new Intent(MainActivity.this, PreferenceActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        Log.e("eroror", "entro en change de la activity");

        mTvNombre.setText("Bienvenido " + sp.getString("PREF_NAME", "Imagina Group"));

        if (sp.getBoolean("PREF_SHOW_IMAGE", true)) {
            mIvImagen.setVisibility(ImageView.VISIBLE);
        } else {
            mIvImagen.setVisibility(ImageView.INVISIBLE);
        }
    }


}
